FROM debian:bullseye

RUN apt-get update && apt-get install -y --no-install-recommends\
    wget \
    ca-certificates \
    git \
    openssh-client \
    rsync \
    curl \
    && rm -rf /var/lib/apt/lists/*
